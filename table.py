#!/usr/bin/python
# -*- coding: utf-8 -*-
from PySide.QtCore import *
from PySide.QtGui import *
import os
import json


DEFAULT = 0
CHECKBOX = 1
settings = QSettings('settings.ini', QSettings.IniFormat)


class TableSteps(QTableWidget):
    def __init__(self, parent):
        QTableWidget.__init__(self)
        self.setItemDelegate(ValidatedItemDelegate())
        self.parent = parent

        self.cols = [
            dict(default=60, name='t', label='How long [s]', widget='default'),
            dict(default=0, name='a1', label='#1 [dB]', widget=DEFAULT),
            dict(default=0, name='a2', label='#2 [dB]', widget=DEFAULT),
            dict(default=0, name='a3', label='#3 [dB]', widget=DEFAULT),
            dict(default=0, name='a4', label='#4 [dB]', widget=DEFAULT),
            dict(default=False, name='ftp_dl', label='FTP DL', widget=CHECKBOX),
            dict(default=False, name='ftp_ul', label='FTP UL', widget=CHECKBOX)
        ]
        labels = [c['label'] for c in self.cols]

        self.setColumnCount(len(labels))
        self.setHorizontalHeaderLabels(labels)

        self.load()

        self.cellClicked.connect(self.refreshWorkspace)
        self.itemChanged.connect(self.cellEdited)

        self.parent.ui.butStepsTmpltSave.clicked.connect(
            lambda: self.saveTemplate(
                self.parent.ui.comboBoxStepsTmplt.currentText()))
        self.parent.ui.butStepsTmpltLoad.clicked.connect(
            lambda: self.loadTemplate(
                self.parent.ui.comboBoxStepsTmplt.currentText()))
        self.parent.ui.butStepsTmpltDel.clicked.connect(
            self.deleteTemplate)

        self.refreshTmpltList(load_default=True)

    def refreshWorkspace(self, row=None, col=None):
        row = row or self.currentRow()
        if self.rowCount() - row == 1:
            self.addRow()

        for r in range(self.rowCount() - 1, 0, -1):
            toRemove = True
            for idc, c in enumerate(self.cols):
                if self.item(r, idc) is None:
                    continue
                if c['widget'] == CHECKBOX:
                    toRemove &= (self.item(r, idc).checkState() ==
                                 Qt.CheckState.Unchecked)
            if toRemove and r > self.currentRow() + 1:
                self.removeRow(r)

    def keyPressEvent(self, event):
        key = event.key()
        super(TableSteps, self).keyPressEvent(event)
        if key == Qt.Key_Return or key == Qt.Key_Enter:
            self.editItem(self.item(self.currentRow(), self.currentColumn()))
            self.refreshWorkspace()
        elif key == Qt.Key_Delete:
            self.removeRow(self.currentRow())
        elif key == Qt.Key_Down:
            self.refreshWorkspace()

    def cellEdited(self, item):
        if self.rowCount() - self.currentRow() == 1:
            return
        row = self.currentRow()
        col = self.currentColumn()
        tmplt = self.cols[col]
        if tmplt['name'] == 'a1':
            txt = self.item(row, col).text()
            if txt and txt != str(tmplt['default']):
                self.item(row, col + 1).setText(txt)
                self.item(row, col + 2).setText(txt)
                self.item(row, col + 3).setText(txt)

    def load(self):
        if self.rowCount() == 0:
            self.addRow()

    def isRowValid(self, r):
        toIgnore = True
        for idc, c in enumerate(self.cols):
            if self.item(r, idc) is None:
                continue
            if c['widget'] == CHECKBOX:
                toIgnore &= (
                    self.item(r, idc).checkState() == Qt.CheckState.Unchecked)
        return not toIgnore

    def saveTemplate(self, name):
        name = name.replace('.json', '')
        if name == '':
            return
        template = []
        for r in range(0, self.rowCount()):
            el = dict()
            for idc, c in enumerate(self.cols):
                if not self.item(r, idc):
                    continue
                if c['widget'] == CHECKBOX:
                    el[c['name']] = (
                        self.item(r, idc).checkState() == Qt.CheckState.Checked)
                else:
                    el[c['name']] = int(self.item(r, idc).text())
            template.append(el)
        with open(os.path.join('sets', name + '.json'), 'wb') as f:
            f.write(json.dumps(template))
        self.refreshTmpltList()

    def loadTemplate(self, name):
        name = name.replace('.json', '')
        if not os.path.exists(os.path.join('sets', name + '.json')):
            return
        for r in range(self.rowCount() - 1, 0, -1):
            self.removeRow(r)
        template = None
        with open(os.path.join('sets', name + '.json'), 'rb') as f:
            template = json.loads(f.read())
        for idr, r in enumerate(template):
            self.addRow()
        for idr, r in enumerate(template):
            for idc, c in enumerate(self.cols):
                newItem = QTableWidgetItem()
                if c['widget'] == CHECKBOX:
                    newItem.setFlags(Qt.ItemIsUserCheckable |
                                     Qt.ItemIsEnabled)
                    newItem.setCheckState(
                        Qt.Checked if r[c['name']] else Qt.Unchecked)
                else:
                    newItem.setText(str(r[c['name']]))
                self.setItem(idr, idc, newItem)
        settings.setValue('steps_tmplt', name)

    def deleteTemplate(self):
        tname = self.parent.ui.comboBoxStepsTmplt.currentText()
        fpath = os.path.join('sets', tname + '.json')
        if not os.path.exists(fpath):
            return

        caption = 'Do you want to delete the template?'
        desc = 'Are you sure to delete "%s"?' % tname
        result = QMessageBox.warning(
            self.parent, caption, desc, QMessageBox.Ok | QMessageBox.Cancel)
        if result == QMessageBox.Cancel:
            return
        os.remove(fpath)
        self.refreshTmpltList()

    def refreshTmpltList(self, load_default=False):
        try:
            os.stat('sets')
        except:
            os.mkdir('sets')

        tmpltsList = self.parent.ui.comboBoxStepsTmplt
        cvalue = tmpltsList.currentText()
        tmpltsList.clear()
        if load_default:
            v = settings.value('steps_tmplt', None)
            cvalue = v if v else cvalue
        for t in [t for t in os.listdir('sets') if '.json' in t]:
            tmpltsList.addItem(t.replace('.json', ''))
        index = tmpltsList.findText(cvalue)
        if index != -1:
            tmpltsList.setCurrentIndex(index)
            self.loadTemplate(cvalue)

    def addRow(self, row=None):
        row = row or self.currentRow() + 1
        self.insertRow(row)
        for idc, c in enumerate(self.cols):
            newItem = QTableWidgetItem()
            if c['widget'] == CHECKBOX:
                newItem.setFlags(Qt.ItemIsUserCheckable |
                                 Qt.ItemIsEnabled)
                newItem.setCheckState(
                    Qt.Checked if c['default'] else Qt.Unchecked)
            else:
                newItem.setText(str(c['default']))
            self.setItem(row, idc, newItem)


class ValidatedItemDelegate(QStyledItemDelegate):
    def createEditor(self, widget, option, index):
        if not index.isValid():
            return 0
        validator = None
        editor = QLineEdit(widget)
        if index.column() == 0:
            validator = QRegExpValidator(QRegExp("\d{1,}"), editor)
        elif 0 < index.column() < 5:
            validator = QRegExpValidator(QRegExp("1?\d{1,2}"), editor)
        if validator:
            editor.setValidator(validator)
        return editor
        # return super(ValidatedItemDelegate, self).createEditor(
        #     widget, option, index)
