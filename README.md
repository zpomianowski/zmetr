# README #

Zmetr to proste narzędzie przydatne podczas tzw. drivetestów. Główne zadania programu to:

* parsowania parametrów radiowych modemów/routerów przez port COM lub za pomocą odpowiednich requestów HTTP
* korelowanie tych danych z próbkami geolokalizacyjnymi (wsparcie dla GPS USB, które działają z użyciem protokołu NMEA0183)
* zapis szybkości transmisji danych z wybranego adaptera sieciowego towarzyszącego testom
* import listy BTS/NodeB/eNodeB
* eksport wyniku testu do pliku CSV
* ekport wyniku testu jako zbiór punktów na mapie (plik .kmz obsługiwany przez GoogleEarth)
* eksport danych zagregowanych wg wybranych parametrów (eksperymentalna funkcjonalność)

![screen of map](misc/example_map_screen.jpg)

Twórca: [Zbigniew Pomianowski <zpomianowski@cyfrowypolsat.pl\>](mailto:zpomianowski@cyfrowypolsat.pl)

Przykładowa mapa kml: [kml](misc/example_map.kmz)

### Generowanie instalatora ###

Do tego użyta jest paczka *cxFreeze*. Konfiguracja w pliku *cx.py*. Listę komend znajdziesz wpisując: ``` python cx.py --help-commmands ```

Generowanie instalatora Windowsowego odbywa się poprzez komendę: ``` python cx.py bdist_wininst ```

Instalator znajdziesz w nowym folderze *dist*. W praktyce *cx_Freeze* embedduje interpreter pythona wraz z potrzebnymi zależnościami. Dla użytkownika w teorii powinno to być bardzo wygodne, bo uwalnia go od kroków poniżej...


### Zależności - środowisko developerskie/debugowe ###
Wylistowane w pliku *requirements.txt*. Problem mogą stanowić większe paczki wymagające procesu kompilacji: **lxml**, **PySide**, **cx-Freeze**. Wówczas warto ściągnąć już [gotowe prekompilowane paczki](http://www.lfd.uci.edu/~gohlke/pythonlibs/).

Instalacja paczek *\*.whl*: ``` pip install <paczka>.whl```

Hurtowa instalacja pozostałych mniejszych zależności: ``` pip install -r requirements.txt ```


### Notka o imporcie danych stacji bazowych ###
Import listy stacji bazowych. Plik musi mieć konkretną strukturę. Przykład:
```
TABLE bts_list
cell_name_plk;BEARING;HEIGHT;ADDRESS;CITY;STREET;site_name;pci;sector_id;CELL_ID;site4g_name;cell_id;earfcn;ON_AIR;latitude_bts;longitude_bts
LB100041;60;19;Filtrowa;Warszawa;ul. Wawelska 46;Filtrowa - Warszawa - ul. Wawelska 46;501;1;1;LB10004;10004;1300;1;52,2175;20,9965
LB100042;160;19;Filtrowa;Warszawa;ul. Wawelska 46;Filtrowa - Warszawa - ul. Wawelska 46;503;2;2;LB10004;10004;1300;1;52,2175;20,9965
LB100043;325;19;Filtrowa;Warszawa;ul. Wawelska 46;Filtrowa - Warszawa - ul. Wawelska 46;502;3;3;LB10004;10004;1300;1;52,2175;20,9965
LB100101;100;27;Srodmiescie;Warszawa;"Al. Jerozolimskie 87; Instytut Rynku Wewnętrznego i Konsumpcji";"Srodmiescie - Warszawa - Al. Jerozolimskie 87; Instytut Rynku Wewnętrznego i Konsumpcji";363;1;1;LB10010;10010;1300;1;52,22698;21,00002
... <- tutaj ciąg dalszy spisu stacji

END
```

### Więcej ###
A więcej znajdziesz na [stronie intranetowej wiki sekcji SAUT](http://saut-test.polsatc/atms/default/wiki/zbyniometr).
Wszelkie formy zgłaszania błędów, sugestii oraz poprawek mile widziane :)
