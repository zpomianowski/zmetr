#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
import os
from time import sleep
from PySide.QtGui import *
from PySide.QtCore import *
from PySide.QtWebKit import *

from mainwindow import *
from com import *
from net import *
from modem import *
from db import LocalStorage
from ge import GE
from charts import Charts
from exporter import Exporter, hexcolor_bts, hexcolor
from idle_dispatcher import ThreadDispatcher
from idle_queue import *
from dal import DAL, Field
from table import TableSteps


DATABASE = 'sqlite://storage.sqlite'
GPS = {'Silabser': 'Holux', 'Prolific': 'Prolific Bu-353'}
MODEMS = {
    'QCUSB_COM': 'HW E398',
    'Prolific': 'ATEL ODU',
    'huawei_cdcacm_AcmSerial0': 'HW E3272',
    'huawei_cdcacm_AcmSerial1': 'HW E3272',
    'huawei_cdcacm_AcmSerial2': 'HW E3272',
    'huawei_cdcacm_AcmSerial3': 'HW E3272',
    'huawei_cdcacm_AcmSerial4': 'HW E3272',
    'huawei_cdcacm_AcmSerial5': 'HW E3272',
    'USBSER000': 'AltairDB'}
WEB = {
    'web_atel100': 'ATEL ODU100/300 WEB',
    'web_atel200': 'ATEL ODU200 WEB'}
ATTS = {
    'VCP0': 'Radio Rack'}
INTERVAL = 1000
VERSION = '2.09.29'
MINOR_V = ''


settings = QSettings('settings.ini', QSettings.IniFormat)
computer_id = os.environ['COMPUTERNAME']
project_name = ''
db = LocalStorage().db


class Window(QMainWindow):
    def __init__(self, parent=None):
        super(Window, self).__init__(parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.setStyles()
        self.dispatcher = ThreadDispatcher(self)
        self.dispatcher.start()

        self.setWindowTitle(self.windowTitle() + ' ' + VERSION + MINOR_V)
        self.setWindowIcon(QIcon('logo.ico'))

        self.tableSteps = TableSteps(self)

        self.ui.verticalLayout_17.addWidget(self.tableSteps)
        self.ui.textStepsRestTime.setValidator(
            QRegExpValidator(QRegExp("\d+"), self.ui.textStepsRestTime))

        # ATT
        self.oAtt = None
        # GPS device
        self.oGps = None
        self.counter = 0
        # Internet adapter
        self.oNet = None
        # Modem
        self.oModem = None
        # Exporter
        self.exporter = None
        self.db = db

        self.timer = QtCore.QTimer(self)
        self.onlineStatus = False

        self.listAll()
        self.timer.start(INTERVAL)
        self.setupControlEvents()
        self.reload()

        # self.minis = Serv(self)
        # self.minis.start()
        self.exporter = Exporter(self, self.db)
        self.charts = Charts(self)
        self.ge = GE(self)

    def setStyles(self):
        self.ui.webView_charts.setObjectName("charts")
        self.ui.webView_charts.setStyleSheet(
            "#charts {color: white;}")

    def setupControlEvents(self):
        isRR220 = self.ui.isRR220
        textStepsRestTime = self.ui.textStepsRestTime
        self.ui.pushButton_runATT.clicked.connect(self.changeATT)
        self.ui.pushButton_runGPS.clicked.connect(self.changeGPS)
        self.ui.pushButton_changeNet.clicked.connect(self.changeNet)
        self.ui.pushButton_changeModem.clicked.connect(self.changeModem)
        self.ui.pushButton_relist.clicked.connect(self.listAll)
        self.ui.lineEdit_projectName.editingFinished.connect(self.changeProjectName)
        self.ui.pushButton_pushSample.clicked.connect(self.takeCustomSample)
        self.ui.pushButton_resetGE.clicked.connect(self.resetGE)
        self.ui.butOpenLogFolder.clicked.connect(
            lambda: QDesktopServices.openUrl(QUrl('logs')))
        isRR220.stateChanged.connect(
            lambda: settings.setValue(
                'isRR220', int(isRR220.checkState() == Qt.Checked)))
        textStepsRestTime.editingFinished.connect(
            lambda: settings.setValue('rest', textStepsRestTime.text()))
        self.timer.timeout.connect(self.update)

        self.ui.checkBox_takeSamples.clicked.connect(self.resetNet)

        self.ui.butStartStepTest.toggled.connect(self.queueTest)

    def resetNet(self):
        if not self.ui.checkBox_takeSamples.isChecked():
            return
        if self.oNet:
            self.oNet.reset()

    def listCOM(self, table, filter):
        table.setSelectionMode(QAbstractItemView.SingleSelection)
        for i in enumerate_serial_ports():
            for dev in [n for n in filter.keys() if re.search(n, str(i[1])) is not None]:
                item = QListWidgetItem("%s (%s)" % (str(i[0]), filter[dev]))
                item.setData(101, str(i[0]))
                item.setData(102, filter[dev])
                table.addItem(item)

    def listWEB(self, table):
        for k, v in WEB.items():
            item = QListWidgetItem("%s" % v)
            item.setData(101, k)
            item.setData(102, v)
            table.addItem(item)

    def listNET(self):
        table = self.ui.listWidget_net
        table.setSelectionMode(QAbstractItemView.SingleSelection)
        for i in enumerate_network_adapters():
            item = QListWidgetItem(i)
            table.addItem(item)

    def listAll(self):
        self.ui.listWidget_ATT.clear()
        self.ui.listWidget_LTE.clear()
        self.ui.listWidget_GPS.clear()
        self.ui.listWidget_net.clear()
        self.listNET()
        self.listCOM(self.ui.listWidget_ATT, ATTS)
        self.listCOM(self.ui.listWidget_LTE, MODEMS)
        self.listWEB(self.ui.listWidget_LTE)
        self.listCOM(self.ui.listWidget_GPS, GPS)

    def reload(self):
        # other
        self.ui.textEdit_rawData.document().setMaximumBlockCount(1000)

        global project_name
        _s = settings.value('project', u'default')
        project_name = _s
        self.ui.lineEdit_projectName.setText(project_name)
        self.ui.label_projectName.setText(project_name)
        self.ui.label_compID.setText(computer_id)

        _s = settings.value('isRR220', 0)
        self.ui.isRR220.setCheckState(
            Qt.Checked if int(_s) == 1 else Qt.Unchecked)

        self.ui.textStepsRestTime.setText(settings.value('rest', '10'))

        # ATTENUATOR
        item = self.getDevice(self.ui.listWidget_ATT, 'com_att')
        if item:
            if self.oAtt:
                self.oAtt.release()
                self.ui.label_ATT.setText('ATT: Disconnected')
            com_speed = int(settings.value('com_att_baudrate', 115200))
            settings.setValue('com_att_baudrate', com_speed)
            com_timeout = int(settings.value('com_att_timeout', 1))
            settings.setValue('com_att_timeout', com_timeout)
            self.oAtt = phyRR(
                item.data(101), com_speed, com_timeout,
                self.ui.isRR220.checkState() == Qt.Checked)
            try:
                self.oAtt.att
                self.ui.label_ATT.setText('ATT: Connected')
            except:
                self.ui.label_ATT.setText('ATT: NOK')
        else:
            self.ui.label_ATT.setText('ATT: NOK')

        # GPS
        item = self.getDevice(self.ui.listWidget_GPS, 'com_gps')
        if item:
            if self.oGps:
                self.oGps.stop()
            com_speed = int(settings.value('com_gps_baudrate', 115200))
            settings.setValue('com_gps_baudrate', com_speed)
            com_timeout = int(settings.value('com_gps_timeout', 1))
            settings.setValue('com_gps_timeout', com_timeout)
            self.oGps = GPSReader(self, com=item.data(101))
            self.oGps.start()

        # NET
        item = self.getDevice(self.ui.listWidget_net, 'adapter')
        if item:
            if self.oNet:
                self.oNet.stop()
            self.oNet = NET(self, item.text())
            self.oNet.start()

        # Modem
        item = self.getDevice(self.ui.listWidget_LTE, 'modem')
        if item:
            if self.oModem:
                self.oModem.stop()
            self.oModem = Modem(self, item.data(101), item.data(102))
            self.oModem.start()


    def getDevice(self, listWidget, key):
        _s = settings.value(key, None)
        if not _s:
            return None
        result = listWidget.findItems(_s, Qt.MatchFlag.MatchStartsWith)
        if len(result) > 0:
            result[0].setSelected(True)
            return listWidget.selectedItems()[0]
        else:
            return None

    def resetGE(self):
        self.ge.stop()
        self.ge = GE(self)

    def changeGPS(self):
        if self.ui.listWidget_GPS.count() == 0:
            return
        com_str = self.ui.listWidget_GPS.selectedItems()[0].data(101)
        settings.setValue('com_gps', com_str)
        self.reload()

    def changeNet(self):
        _s = self.ui.listWidget_net.selectedItems()[0].text()
        settings.setValue('adapter', _s)
        self.reload()

    def changeATT(self):
        if self.ui.listWidget_ATT.count() == 0:
            return
        com_str = self.ui.listWidget_ATT.selectedItems()[0].data(101)
        settings.setValue('com_att', com_str)
        self.reload()

    def changeModem(self):
        if self.ui.listWidget_LTE.count() == 0:
            return
        _s = self.ui.listWidget_LTE.selectedItems()[0].text()
        settings.setValue('modem', _s)
        self.reload()

    def changeProjectName(self):
        global project_name
        project_name = self.ui.lineEdit_projectName.text()
        self.ui.label_projectName.setText(project_name)
        settings.setValue('project', project_name)

    def update(self):
        if self.ui.checkBox_takeSamples.isChecked():
            idle_loop.add(self.takeSample)
        if self.ui.checkBox_geTrace.isChecked():
            self.ge.my_hub.move(self.oGps, self.ui.slider_zoom.value())
        if self.charts:
            if self.charts.my_hub:
                if self.oNet:
                    self.charts.my_hub.update('DL', self.oNet.speed['dl'])
                    self.charts.my_hub.update('UL', self.oNet.speed['ul'])
                if self.oModem:
                    keys = ['RSSI', 'RSRP', 'RSRQ']
                    for k in keys:
                        if self.oModem.data.has_key(k):
                            self.charts.my_hub.update(
                                k, float(self.oModem.data[k]))

    def updateUI(self):
        self.ui.textEdit_rawData.append(self.oModem.current)

    def takeCustomSample(self):
        idle_loop.add(lambda: self.takeSample(
            sample=self.ui.lineEdit_pushSample.text(),
            desc=self.ui.textEdit_pushSample.toPlainText()
            ))

    def takeSample(self, sample='', desc='', forceGPS=True):
        from datetime import datetime

        db = LocalStorage().db
        global project_name
        global computer_id

        data = {
            'utc': datetime.utcnow(),
            'computer_id': computer_id,
            'project_name': project_name,
            'sample': sample,
            'desc': desc,
            'speed': 0
            }

        if forceGPS:
            if not self.oGps:
                return
            if not self.oGps.status:
                return
            if not self.oGps.data.has_key('GPRMC'):
                return
            if self.oGps.data['GPRMC'].data_validity != 'A':
                return

            data.update({
                'lat': self.oGps.data['GPRMC'].lat,
                'lat_dir': self.oGps.data['GPRMC'].lat_dir,
                'lon': self.oGps.data['GPRMC'].lon,
                'lon_dir': self.oGps.data['GPRMC'].lon_dir})

        if self.oNet:
            data.update({
                'dl': self.oNet.dl,
                'ul': self.oNet.ul,
                'internet': self.oNet.oc.getStatus()})

        if self.oAtt and not forceGPS:
            data.update({('att%s' % str(idx + 1).zfill(2)): x
                   for (idx, x) in enumerate(self.oAtt.att)})

        currentPoint = None
        if self.oModem:
            extra_data = self.oModem.data.copy()
            for k in extra_data:
                extra_data[k.lower().replace(' ', '_')] = extra_data.pop(k)
                if k == 'SITE ID': currentPoint = extra_data['site_id']
            data = dict(data.items() + extra_data.items())

        for i in range(3):
            try:
                db.samples.insert(**data)
                db.commit()
                break
            except Exception as err:
                print err
                print 'DB write error (%d)' % i
        self.counter += 1
        self.ui.checkBox_takeSamples.setText('Samples: %d' % self.counter)
        if sample != '':
            self.info('Info', 'Sample taken.')

        if not self.ge.my_hub or not forceGPS:
            return
        if not self.oNet:
            return
        value = hexcolor(self.oNet.speed['dl'])
        if currentPoint and self.oModem.status > 0:
            value = hexcolor(extra_data['site_id'] % 13, max=13)
        self.ge.my_hub.update(self.oGps, value[:-2],
            '<br>'.join(['%s: %s' % (k, str(v)) for k, v in data.items()]))

    def wait(self, seconds):
        for s in xrange(seconds):
            sleep(1)
            if not self.ui.butStartStepTest.isChecked():
                raise Exception()

    def queueTest(self, checked):
        if checked:
            idle_loop.add(self.stepTest)

    def stepTest(self):
        t = self.tableSteps
        t.setEditTriggers(QAbstractItemView.NoEditTriggers)
        try:
            if not self.oNet or not self.oAtt:
                raise Exception('Net or Atteunator not set!')
            for r in range(t.rowCount()):
                if not t.isRowValid(r):
                    continue
                t.selectRow(r)

                QApplication.postEvent(self, ZEvent(
                    lambda: QSound.play("sound/jump.wav")))

                # guard time gap
                self.wait(int(self.ui.textStepsRestTime.text()))
                self.oNet.reset()

                for idc, c in enumerate(t.cols):
                    if c['widget'] == 1:
                        continue
                    col_name = c['name']
                    value = int(t.item(r, idc).text())
                    if 'a' in col_name:
                        self.oAtt.set_att(int(col_name[-1]), value)

                self.wait(int(t.item(r, 0).text()))

                self.takeSample(forceGPS=False)
            QApplication.postEvent(self, ZEvent(
                lambda: QSound.play("sound/stop.wav")))
        except Exception as err:
            import traceback
            import serial
            if str(err) != '':
                details = traceback.print_exc(file=sys.stdout)
                if details:
                    details = str(err) + details
                else:
                    details = str(err)
                QApplication.postEvent(self, ZEvent(
                    lambda: self.info(str(err), details)))
            QApplication.postEvent(self, ZEvent(
                lambda: QSound.play("sound/interrupt.wav")))
            if type(err) == serial.serialutil.SerialException:
                QApplication.postEvent(self, ZEvent(
                    lambda: self.ui.label_ATT.setText('ATT: NOK')))
        finally:
            t.setEditTriggers(QAbstractItemView.AnyKeyPressed)
            self.ui.butStartStepTest.setChecked(False)


    def info(self, caption, desc):
        QApplication.postEvent(
            self,
            ZEvent(lambda: QMessageBox.information(
                self, caption, desc, QMessageBox.Ok)))

    def closeEvent(self, event):
        self.ui.butStartStepTest.setChecked(False)
        self.hide()
        self.allStop()
        # self.minis.stop()

    def allStop(self):
        self.dispatcher.stop()
        self.timer.stop()
        if self.oGps:
            self.oGps.stop()
        if self.oNet:
            self.oNet.stop()
        if self.oModem:
            self.oModem.stop()
        self.ge.stop()

    def allStart(self):
        self.dispatcher.start()
        self.timer.start()
        self.oGps.start()
        self.oNet.start()
        self.oModem.start()
        self.ge.start()

    def customEvent(self, event):
        if callable(event.callback):
            event.callback()

if __name__ == "__main__":
    import qdarkstyle
    app = QApplication(sys.argv)
    app.setStyleSheet(qdarkstyle.load_stylesheet())
    mySW = Window()
    mySW.show()

    sys.exit(app.exec_())
