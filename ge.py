#!/usr/bin/python
# -*- coding: utf-8 -*-

from PySide.QtCore import *
from PySide.QtWebKit import *

from idle_dispatcher import ThreadDispatcher
from idle_queue import *
from com import convert_gps_coords

class SautWebPage(QWebPage):
    def __init__(self, parent):
        QWebPage.__init__(self, parent)

    def userAgentForUrl(self, url):
        return 'Mozilla/5.0 (Windows NT 5.1; rv:13.0) Gecko/20100101 Firefox/13.0.1'

    def javaScriptConsoleMessage(self, msg, line, source):
        print 'WEBVIEW: %s line %d: %s' % (source, line, msg)

class GE(QObject):
    def __init__(self, parent):
        QObject.__init__(self, parent)
        self.dispatcher = ThreadDispatcher(self, q=idle_loop2)
        self.dispatcher.start()
        self.timer = QTimer(self)
        self.my_hub = None

        self.parent = parent
        self.page = SautWebPage(self.parent.ui.tab_view)
        self.page.loadFinished.connect(self.onLoad)
        self.page.settings().setAttribute(QWebSettings.PluginsEnabled, True);
        self.parent.ui.webView.setPage(self.page)
        self.page.mainFrame().load(QUrl('ge/earth.html'))

    def onLoad(self):
        self.my_hub = Hub()
        self.page.mainFrame().addToJavaScriptWindowObject("my_hub", self.my_hub)
        self.page.mainFrame().evaluateJavaScript("initBridge();")
        self.timer.timeout.connect(self.update)
        self.timer.start(1000)
        # idle_loop2.add(self.my_hub.load_files)

    def update(self):
        self.my_hub.show(self.parent.oGps)

    def start(self):
        self.dispatcher.start()

    def stop(self):
        self.dispatcher.stop()
        print 'GE stop'

class Hub(QObject):
    def __init__(self):
        super(Hub, self).__init__()
        self.lat = 52
        self.lon = 21

    def move(self, gps, zoom):
        self.on_move.emit(self.lat, self.lon, zoom*(zoom/10))

    def show(self, gps):
        try:
            c = convert_gps_coords(gps)
            self.lat = c[0]
            self.lon = c[1]
            self.on_show.emit(self.lat, self.lon)
        except: pass

    def update(self, gps, color, desc):
        c = convert_gps_coords(gps)
        self.lat = c[0]
        self.lon = c[1]
        self.on_update.emit(self.lat, self.lon, color, desc)

    def load_files(self):
        files = []
        for f in os.listdir(WD + '/ge'):
            if f[-3:] in ['kml', 'kmz']: files.append(f)
        for f in files:
            import re
            opacity = 1.0
            m = re.search(r'op(\d)', f)
            if m:
                opacity = float(m.group(1))/10
            import time
            time.sleep(1)
            self.on_kml.emit("http://127.0.0.1:45678/ge/"+f, opacity)

    on_update = Signal(float, float, str, str)
    on_kml  = Signal(str, float)
    on_move = Signal(float, float, int)
    on_show = Signal(float, float)
