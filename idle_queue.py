#!/usr/bin/python
# -*- coding: utf-8 -*-

import Queue
import os

WD = os.getcwd()

class IdleLoop(Queue.Queue):
    def __init__(self):
        Queue.Queue.__init__(self)

    def add(self, func, *args, **kwargs):
        def idle():
            return func(*args, **kwargs)
        self.put(idle)

idle_loop = IdleLoop()
idle_loop2 = IdleLoop()