#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
from cx_Freeze import setup, Executable
from zmetr import VERSION

# Dependencies are automatically detected, but it might need fine tuning.
build_exe_options = {
    "packages":
        ["os", "pynmea", "atexit", "serial.win32",
         "PySide.QtNetwork", "PySide.QtWebKit", "lxml"],
    "excludes": ["tkinter"],
    "optimize": 2,
    # "append_script_to_exe": True,
    # "include_in_shared_zip": True,
    "include_files": ["ge", "misc", "sound", "logo.ico"]}
    # "icon": "logo.ico"}

shortcut_table = [
    ("DesktopShortcut",        # Shortcut
     "DesktopFolder",          # Directory_
     "Zmetr",                  # Name
     "TARGETDIR",              # Component_
     "[TARGETDIR]\zmetr.exe",  # Target
     None,                     # Arguments
     None,                     # Description
     None,                     # Hotkey
     None,                     # Icon
     None,                     # IconIndex
     None,                     # ShowCmd
     'TARGETDIR'               # WkDir
     )
    ]

# GUI applications require a different base on Windows (the default is for a
# console application).
base = None
if sys.platform == "win32":
    base = "Win32GUI"

setup(name="Zmetr",
      version=VERSION,
      description="Zmetr - drivetest tool / RF step tests",
      options={
          "build_exe": build_exe_options,
          "bdist_msi": {
              "initial_target_dir": "c:\zmetr",
              'data': {"Shortcut": shortcut_table}
          }},
      executables=[Executable("zmetr.py", base=base, icon="logo.ico")])
