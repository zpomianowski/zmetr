#!/usr/bin/python
# -*- coding: utf-8 -*-

from PySide.QtGui import *
from PySide.QtCore import *

from idle_queue import *

class ThreadDispatcher(QThread):
    def __init__(self, parent, q = None):
        QThread.__init__(self)
        self.parent = parent
        if q is None:
            self.loop = idle_loop
        else:
            self.loop = q

    def run(self):
        while True:
            fcallback = self.loop.get()
            if fcallback is None:
                break
            callback = fcallback()
            QApplication.postEvent(self.parent, ZEvent(callback))

    def stop(self):
        self.loop.put(None)
        self.wait()
        print 'Dispatcher stop'

class ZEvent(QEvent):
    EVENT_TYPE = QEvent.Type(QEvent.registerEventType())

    def __init__(self, callback):
        QEvent.__init__(self, ZEvent.EVENT_TYPE)
        self.callback = callback