#!/usr/bin/python
# -*- coding: utf-8 -*-
import psutil
import httplib
import urllib
import time

import urllib2
from PySide.QtGui import *
from PySide.QtCore import *
from idle_dispatcher import *
from collections import deque

httplib.HTTPConnection.debuglevel = 1


def enumerate_network_adapters():
    return psutil.net_io_counters(pernic=True).keys()


class NET(QThread):
    def __init__(self, parent=None, interface=None, interval=1000):
        self.parent = parent
        self._flag = True
        self.interface = interface
        self.interval = interval / 1000.
        self._stats = dict(sc=0, so=0, rc=0, ro=-1)
        self.speed = dict(dl=0, ul=0)
        QThread.__init__(self)

        self.oc = OnlineChecker(self)
        self.oc.start()

        self.__dlQ = deque(maxlen=1e6//interval)
        self.__ulQ = deque(maxlen=1e6//interval)

    @property
    def dl(self):
        return self.__getQAvg('dl')

    @property
    def ul(self):
        return self.__getQAvg('ul')

    def reset(self):
        self.__dlQ.clear()
        self.__ulQ.clear()

    def __getQAvg(self, key='dl'):
        return self.speed[key]
        if key == 'dl':
            q = self.__dlQ
        if key == 'ul':
            q = self.__ulQ
        count = 0
        sum = 0
        while True:
            try:
                sum += q.pop()
                count += 1
            except IndexError:
                break
        if count == 0:
            return self.speed[key]
        return sum / count

    def run(self):
        while self._flag:
            import time
            start_time = time.time()
            time.sleep(self.interval)
            if self.interface:
                _stats = psutil.net_io_counters(pernic=True)[self.interface]
                self._stats['sc'] = _stats[0]
                self._stats['rc'] = _stats[1]
                if self._stats['ro'] != -1:
                    time_m_fac = (time.time() - start_time)
                    self.speed['dl'] = round(
                        ((self._stats['rc'] - self._stats['ro']) * 8) /
                        (time_m_fac * 1048576), 2)
                    self.speed['ul'] = round(
                        ((self._stats['sc'] - self._stats['so']) * 8) /
                        (time_m_fac * 1048576), 2)
                self._stats['so'] = self._stats['sc']
                self._stats['ro'] = self._stats['rc']
            if self.parent:
                QApplication.postEvent(self.parent, ZEvent(self.update))

    def stop(self):
        self._flag = False
        self.wait()
        self.oc.stop()
        print 'NET stop'

    def update(self):
        self.__dlQ.append(self.speed['dl'])
        self.__ulQ.append(self.speed['ul'])
        dl = '%s [Mbps]' % str(self.speed['dl'])
        ul = '%s [Mbps]' % str(self.speed['ul'])
        self.parent.ui.label_DL.setText('DL: %s' % dl)
        self.parent.ui.label_UL.setText('UL: %s' % ul)
        self.parent.ui.label_internet.setText(
            'Internet: OK' if self.oc.getStatus() else
            'Internet: Disconnected!')


class OnlineChecker(QThread):
    def __init__(self, parent):
        QThread.__init__(self, parent)
        self.parent = parent
        self._flag = True
        self.__status = 0

    def run(self):
        while self._flag:
            try:
                import time
                time.sleep(1)
                response = urllib2.urlopen('http://212.77.100.101', timeout=10)
                self.__status = 1
            except Exception as er:
                self.__status = 0

    def getStatus(self):
        return self.__status == 1

    def stop(self):
        self._flag = False
        self.wait()
        print 'OnlineChecker stop'


class WebDataMiner(QThread):
    def __init__(self, parent, url):
        QThread.__init__(self, parent)
        self.parent = parent
        self.url = url
        self._flag = True

        self.__headers = {
            "Accept": "text/html,application/xhtml+xml,"
            "application/xml;q=0.9,image/webp,*/*;q=0.8",
            "Accept-Encoding": "gzip, deflate, sdch",
            "Accept-Language": "pl,en;q=0.8,en-GB;q=0.6,de;q=0.4",
            "Cache-Control": "max-age=0",
            "Host": "192.168.100.1",
            "Upgrade-Insecure-Requests": 1,
            "User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) "
                          "AppleWebKit/537.36 (KHTML, like Gecko) "
                          "Chrome/58.0.3029.81 Safari/537.36"
            }

        proxy = urllib2.ProxyHandler({})
        self.opener = urllib2.build_opener(proxy)
        urllib2.install_opener(self.opener)

    def run(self):
            while self._flag:
                try:
                    import time
                    time.sleep(1)
                    # request = urllib2.Request(
                    #     self.url, headers=self.__headers)
                    response = urllib2.urlopen(self.url, timeout=10)
                    idle_loop2.add(lambda: self.parent.parse(response.read()))
                except Exception as er:
                    self.parent.status = -3
                    QApplication.postEvent(
                        self.parent.parent, ZEvent(
                            lambda: self.parent.update(self.parent.status)))

    def stop(self):
        self._flag = False
        self.wait()
        print 'WebDataMiner stop'


class Serv(QThread):
    def __init__(self, parent):
        self.parent = parent
        QThread.__init__(self)
        self.httpd = None

    def run(self):
        import SimpleHTTPServer
        import SocketServer
        PORT = 45678
        Handler = SimpleHTTPServer.SimpleHTTPRequestHandler
        self.httpd = SocketServer.TCPServer(("", PORT), Handler)
        self.httpd.serve_forever()

    def stop(self):
        self.httpd.shutdown()
        self.wait()
        print 'Server stop'

if __name__ == "__main__":
    enumerate_network_adapters()
