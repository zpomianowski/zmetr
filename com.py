#!/usr/bin/python
# -*- coding: utf-8 -*-
import Queue
import threading
import itertools
import serial
from abc import ABCMeta, abstractproperty
import _winreg as winreg
from pynmea.streamer import NMEAStream
from PySide.QtGui import *
from PySide.QtCore import *
from idle_dispatcher import *#ZEvent


def enumerate_serial_ports():
    path = 'HARDWARE\\DEVICEMAP\\SERIALCOMM'
    try:
        key = winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, path)
    except WindowsError:
        raise IterationError

    for i in itertools.count():
        try:
            val = winreg.EnumValue(key, i)
            yield (val[1], val[0])
        except EnvironmentError:
            break
    yield ('http://192.168.100.1/cgi-bin/ltestatus.cgi?Command=Status', 'atel')

def convert_gps_coords(gps):
    if not gps.data.has_key('GPRMC'): return (52, 20)
    lat = gps.data['GPRMC'].lat
    lat_dir = gps.data['GPRMC'].lat_dir
    lon = gps.data['GPRMC'].lon
    lon_dir = gps.data['GPRMC'].lon_dir
    return convert_coords(lat, lat_dir, lon, lon_dir)

import math
def distance(start,  end):
    start_long = math.radians(recalculate_coordinate(start[0],  'deg'))
    start_latt = math.radians(recalculate_coordinate(start[1],  'deg'))
    end_long = math.radians(recalculate_coordinate(end[0],  'deg'))
    end_latt = math.radians(recalculate_coordinate(end[1],  'deg'))
    d_latt = end_latt - start_latt
    d_long = end_long - start_long
    a = math.sin(d_latt/2)**2 + math.cos(start_latt) * math.cos(end_latt) * math.sin(d_long/2)**2
    c = 2 * math.atan2(math.sqrt(a),  math.sqrt(1-a))
    return 6371 * c

def convert_coords(lat, lat_dir, lon, lon_dir):
    lat_d = float(lat[:-7])
    lat_m = float(lat[-7:])
    lat = lat_d + lat_m / 60

    if lat_dir == 'S': lat = -lat

    lon_d = float(lon[:-7])
    lon_m = float(lon[-7:])
    lon = lon_d + lon_m / 60

    if lon_dir == 'W': lon = -lon
    return (lat, lon)

class GPSReader(QThread):
    def __init__(self, parent=None, baudrate = 4800, com = 'COM1', timeout = 1):
        self.ser = serial.Serial()
        self.ser.baudrate = baudrate
        self.ser.port = com
        self.ser.timeout = timeout
        self.parent = parent
        self.status = 0

        self.streamer = NMEAStream()
        self.data = {}
        self._flag = True
        QThread.__init__(self)

    def run(self):
        _l = self.parent.ui.label_GPS if self.parent else None
        while self._flag:
            if not self.ser.isOpen():
                try:
                    self.ser.open()
                    self.status = 1
                except:
                    self.status = -1
                    import time
                    time.sleep(3)

            else:
                lock = threading.Lock()
                try:
                    lock.acquire()
                    for o in self.streamer.get_objects(self.ser.readline()):
                        self.data[o.sen_type] = o
                except Exception:
                    self.status = -2; self._flag = False
                lock.release()
                if self.parent:
                    QApplication.postEvent(self.parent, ZEvent(self.update))

    def stop(self):
        self._flag = False
        self.wait()
        self.ser.close()
        print 'GPS stop'

    def update(self):
        nb = 0
        data = self.data.copy()
        for k in data:
            nb += len(data[k].parse_map)
        table = self.parent.ui.tableWidget_GPS
        table.horizontalHeader().setStretchLastSection(True)
        table.setHorizontalHeaderItem(0, QTableWidgetItem('Parameter'))
        table.setHorizontalHeaderItem(1, QTableWidgetItem('Value'))
        table.setRowCount(nb)
        table.setColumnCount(2)
        row_nb = 0
        for k in data:
            for p in data[k].parse_map:
                if p[1] == 'data_validity':
                    if getattr(data[k], p[1]) != 'A':
                        self.status = -3
                try:
                    item1 = QTableWidgetItem(k + " - " + p[1])
                    item2 = QTableWidgetItem(getattr(data[k], p[1]))
                    item1.setFlags(Qt.ItemIsSelectable|Qt.ItemIsEnabled)
                    item2.setFlags(Qt.ItemIsSelectable|Qt.ItemIsEnabled)
                    table.setItem(row_nb, 0, item1)
                    table.setItem(row_nb, 1, item2)
                    row_nb += 1
                except Exception:
                    pass

        _l = self.parent.ui.label_GPS
        if self.status == 0:  _l.setText('GPS: Not connected!')
        if self.status == -1: _l.setText('GPS: Can\'t connect!')
        if self.status == -2: _l.setText('GPS: Connection lost!')
        if self.status == -3: _l.setText('GPS: Weak fix!')
        if self.status == 1:  _l.setText('GPS: OK')

import struct
import datetime
def gps_time(timestamp):
    unpacked = struct.unpack('<q', timestamp)[0]
    seconds = unpacked / 52428800
    microseconds = int(round((unpacked % 52428800) / 52.428800, 0))
    return GPS_EPOCH + datetime.timedelta(seconds=seconds, microseconds=microseconds)

FAIL_LIMIT = 5
class AT(QThread):
    def __init__(self, parent=None ,baudrate=57600, com = 'COM47',
                 timeout=1, writeTimeout=1):
        QThread.__init__(self)
        self.ser = serial.Serial()
        self.ser.baudrate = 921600
        self.ser.port = com
        self.ser.timeout = timeout
        self.ser.writeTimeout = writeTimeout
        self.parent = parent
        self.status = 0
        self.fails = 0

        self.qcmd = Queue.Queue()
        self._flag = True

    def add(self, cmd, flag=False):
        if self.ser.isOpen():
            self.qcmd.put(cmd)
        if flag: self.qcmd.put(cmd)

    def run(self):
        string2 = ''
        string1 = ''
        while self._flag:
            if not self.ser.isOpen():
                try:
                    self.ser.open()
                    self.status = 1
                except Exception as ex:
                    #print 'Error', ex
                    self.status = -1
                    import time
                    time.sleep(10)
            else:
                tries = 0
                string2 = string1
                string1 = ''
                while self._flag and len(string1) < 2 and tries < 3:
                    try:
                        if not self.qcmd.empty():
                            cmd = self.qcmd.get(False)
                            self.ser.write(cmd+'\r\n')
                            string2 = cmd
                        data = self.ser.readline()
                        data = data.replace('\r\n', '')
                        data = data.replace('\r', '')
                        data = data.replace('\n', '')
                        string1 += data
                        tries += 1
                    except Exception as ex:
                        # print ex
                        self.status = -2; self._flag = False
                if self.parent:
                    if len(string1) == 0: self.fails += 1
                    else: self.fails = 0
                    if self.fails == FAIL_LIMIT: self.status = -3
                    #print string1
                    idle_loop.add(lambda: self.parent.parse('%s\n%s' % (string2, string1)))
                    #self.parent.parse('%s\n%s' % (string2, string1))
                    QApplication.postEvent(self.parent.parent, ZEvent(lambda: self.parent.update(self.status)))

    def stop(self):
        self._flag = False
        self.wait()
        self.ser.close()
        print 'AT stop'


class GenericAttenuator(object):
    __metaclass__ = ABCMeta

    def __init__(self, dev):
        super(GenericAttenuator, self).__init__()

    @abstractproperty
    def att(self):
        pass


class phyRR(GenericAttenuator):
    MIN = 0
    MAX = 110

    def __init__(self, dev, baudrate, timeout, isRR220=False):
        super(phyRR, self).__init__(dev)
        self.driver = serial.Serial()
        self.driver.port = dev
        self.driver.baudrate = baudrate
        self.driver.timeout = timeout
        self.read_termination = '\r\n'
        self.write_termination = '\r\n'
        self.__nb_of_channels = 4
        self.__isRR220 = isRR220

    def get_info(self):
        return 'Radio Rack 4 4-221'

    def __query(self, cmd):
        if not self.driver.isOpen():
            self.driver.open()
        self.driver.write(cmd + self.write_termination)
        return self.driver.readline()

    def set_att(self, channel, value):
        if not isinstance(channel, int):
            raise TypeError('channel has to be integer type')
        value = value * 2 if self.__isRR220 else value
        if not isinstance(value, int):
            raise TypeError('attenuation has to be integer type')
        if not 1 <= channel <= self.__nb_of_channels:
            raise ValueError(
                'channel has to be between: 1-%d' % self.__nb_of_channels)
        value = min(max(value, phyRR.MIN), phyRR.MAX)
        self.__query('ATT %d %d' % (channel, value))

    @property
    def att(self):
        atts = self.__query('ATT').split(';')
        try:
            return [int(a.split(' ')[-1]) for a in atts]
        except:
            raise Exception('No valid connection with Radio Rack')

    @att.setter
    def att(self, value):
        if not isinstance(value, int):
            raise TypeError('"att" has to be integer type')
        value = value * 2 if self.__isRR220 else value
        if not phyRR.MIN <= value <= phyRR.MAX:
            raise ValueError('"att" has to be between: %d-%d dB' % (
                phyRR.MIN, phyRR.MAX))
        x = []
        for i in range(self.__nb_of_channels):
            x.append('ATT %d %d' % (i, value))
        cmd = ';'.join(x)
        self.__query(cmd)

    def release(self):
        self.driver.close()
