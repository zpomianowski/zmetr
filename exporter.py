#!/usr/bin/python
# -*- coding: utf-8 -*-

from PySide.QtCore import *
from PySide.QtGui import *
import math

from idle_queue import *
from idle_dispatcher import ZEvent
from db import LocalStorage

settings = QSettings('settings.ini', QSettings.IniFormat)

class Exporter(QThread):
    def __init__(self, parent, db):
        QThread.__init__(self, parent)
        self.parent = parent
        self.db = db

        exclude = []# ['id', 'utc', 'lat', 'lat_dir', 'lon', 'lon_dir', 'computer_id', 'project_name', 'internet']

        table = self.parent.ui.listWidget_exportColumns
        for f in db.samples.fields:
            if f in exclude: continue
            item = QListWidgetItem(f)
            v = settings.value(f, '2')
            if v == '2': item.setCheckState(Qt.Checked)
            else: item.setCheckState(Qt.Unchecked)
            table.addItem(item)
        v = settings.value(f, '2')

        item = self.parent.ui.lineEdit_exportFile
        newname = settings.value('exportFile', 'default')
        item.setText(newname)
        self.parent.ui.label_exportName.setText('Save as: %s' % newname)
        item.editingFinished.connect(self.fileChanged)

        items = [
            (self.parent.ui.checkBox_exportComputerID, self.parent.ui.comboBox_exportComputerID),
            (self.parent.ui.checkBox_exportProject, self.parent.ui.comboBox_exportProject),
            (self.parent.ui.checkBox_exportFrom, self.parent.ui.dateTimeEdit_exportFrom),
            (self.parent.ui.checkBox_exportTo, self.parent.ui.dateTimeEdit_exportTo)
        ]

        i1 = self.parent.ui.checkBox_exportComputerID
        i2 = self.parent.ui.comboBox_exportComputerID
        i1.stateChanged.connect(lambda: self.checkBoxChanged(i1, i2))

        i3 = self.parent.ui.checkBox_exportProject
        i4 = self.parent.ui.comboBox_exportProject
        i3.stateChanged.connect(lambda: self.checkBoxChanged(i3, i4))

        i5 = self.parent.ui.checkBox_exportFrom
        i6 = self.parent.ui.dateTimeEdit_exportFrom
        i5.stateChanged.connect(lambda: self.checkBoxChanged(i5, i6))

        i7 = self.parent.ui.checkBox_exportTo
        i8 = self.parent.ui.dateTimeEdit_exportTo
        i7.stateChanged.connect(lambda: self.checkBoxChanged(i7, i8))

        table.itemChanged.connect(self.itemChanged)
        self.parent.ui.pushButton_exportCSV.clicked.connect(lambda: idle_loop2.add(self.export2csv))
        self.parent.ui.pushButton_exportKML.clicked.connect(lambda: idle_loop2.add(self.export2kml))
        self.parent.ui.pushButton_importBtsList.clicked.connect(self.importBtsList)
        self.parent.ui.pushButton_delete.clicked.connect(self.delete)

        # aggregate
        aggregate = self.parent.ui.comboBox_aggregate
        for f in ['distance','dl','ul', 'rsrq','rsrp','rssi','speed']:
            aggregate.addItem(f)
        self.parent.ui.pushButton_aggregate.clicked.connect(lambda: idle_loop2.add(lambda: self.aggregate(
                                                self.parent.ui.comboBox_aggregate.currentText(),
                                                self.parent.ui.doubleSpinBox_aggregate.value())))

    def aggregate(self, field, prec=1):
        db        = self.db;
        q = self.query()
        query     = q['q']
        to_select = q['s']
        samples = db(query).select(*to_select, orderby=field)
        QApplication.postEvent(self, ZEvent(lambda: self.setPB(0, 'aggregating %s' % field.upper(), len(samples))))

        shift = float(prec) / 2

        bins = dict()
        for p in samples:
            if not p.has_key(field): break
            if not p[field]: continue
            i = str(int((float(p[field]) + shift) / prec) * prec)
            if not bins.has_key(i): bins[i] = []
            bins[i].append(p)

        import collections
        od = collections.OrderedDict(sorted(bins.items()))

        result = collections.OrderedDict()
        i = 0
        for bin, content in od.items():
            result[bin] = dict()
            counter = 0
            for row in content:
                counter += 1
                i += 1
                QApplication.postEvent(self, ZEvent(lambda: self.setPB(i)))
                for f, v in row.items():
                    if not result[bin].has_key(f): result[bin][f] = 0
                    try:
                        result[bin][f] += v
                    except: result[bin][f] = None
            for key in result[bin]:
                try:
                    result[bin][key] /= counter
                except: pass

        import csv
        with open('logs/%s_%s_prec%s.csv' % (self.parent.ui.lineEdit_exportFile.text(), field, prec), 'wb') as f:
            writer = csv.writer(f, dialect=csv.excel, delimiter=';')
            flag = True
            for k,v in result.items():
                if flag:
                    writer.writerow([field] + result[k].keys())
                    flag = False
                writer.writerow([k] + v.values())
        QApplication.postEvent(self, ZEvent(lambda: self.setPB(100, 'aggregation', 100)))

    def importBtsList(self):
        import os
        d = os.getcwd()
        filename = QFileDialog.getOpenFileName(self.parent,"Import BTS list", "logs", "DB CSV Files (*.csv)")[0]
        os.chdir(d)
        if filename:
            idle_loop2.add(lambda: self.importFromCSV(filename))

    def importFromCSV(self, filename):
        db = LocalStorage().db
        QApplication.postEvent(self, ZEvent(lambda: self.setPB(10, filename)))
        try:
            db.bts_list.truncate()
            db.import_from_csv_file(open(filename))
            db.commit()
            QApplication.postEvent(self, ZEvent(lambda: self.setPB(100, filename)))
        except Exception as ex:
            caption = 'Error occured'
            desc =  """
            Error: %s

            Example (how CSV file should be like):

            TABLE bts_list
            zone;name;site4g_name ... numlongitude;day
            1;LB500561;LB50056; ... 21;044524;2013-05-08 00:00
            1;LB500561;LB50056; ... 21;044524;2013-05-08 00:00
            ...
            1;LB500561;LB50056; ... 21;044524;2013-05-08 00:00

            END

            """ % ex
            QApplication.postEvent(self, ZEvent(lambda: self.error(caption, desc)))
            QApplication.postEvent(self, ZEvent(lambda: self.setPB(0, filename)))

    def error(self, caption, desc):
        QMessageBox.warning(self.parent, caption, desc, QMessageBox.Ok)

    def checkBoxChanged(self, item, item2):
        if item.checkState() == Qt.Checked:
            item2.setEnabled(True)
            if item2 == self.parent.ui.comboBox_exportProject:
                item2.clear()
                projects = self.db().select('samples.project_name', orderby='samples.project_name', groupby='samples.project_name')
                for p in projects:
                    item2.addItem(p.project_name)
            if item2 == self.parent.ui.comboBox_exportComputerID:
                item2.clear()
                comps = self.db().select('samples.computer_id', orderby='samples.computer_id', groupby='samples.computer_id')
                for c in comps:
                    item2.addItem(c.computer_id)
        else: item2.setEnabled(False)

    def delete(self):
        caption = 'Removing set of records'
        desc = 'Are you sure?'
        result = QMessageBox.warning(self.parent, caption, desc, QMessageBox.Ok | QMessageBox.Cancel)
        if result == QMessageBox.Cancel: return
        caption = 'Removing set of records'
        desc = 'Are you.. really, really sure?'
        result = QMessageBox.warning(self.parent, caption, desc, QMessageBox.Ok | QMessageBox.Cancel)
        if result == QMessageBox.Cancel: return
        db = self.db
        db(self.query()['q']).delete()
        db.commit()
        self.parent.ui.comboBox_exportProject.clear()
        self.parent.ui.comboBox_exportComputerID.clear()
        projects = self.db().select('samples.project_name', orderby='samples.project_name', groupby='samples.project_name')
        for p in projects:
            self.parent.ui.comboBox_exportProject.addItem(p.project_name)
        comps = self.db().select('samples.computer_id', orderby='samples.computer_id', groupby='samples.computer_id')
        for c in comps:
            self.parent.ui.comboBox_exportComputerID.addItem(c.computer_id)

    def fileChanged(self):
        newname = self.parent.ui.lineEdit_exportFile.text()
        settings.setValue('exportFile', newname)
        self.parent.ui.label_exportName.setText('Save as: %s' % newname)

    def itemChanged(self, item):
        state = item.checkState()
        settings.setValue(item.text(), state)

    def run(self):
        pass

    def export2csv(self):
        db = LocalStorage().db
        filename = 'logs/%s.csv' % self.parent.ui.lineEdit_exportFile.text()
        QApplication.postEvent(self, ZEvent(lambda: self.setPB(1, filename, 100)))
        q = self.query()
        for i in ['rssi', 'rsrq', 'rsrp']:
            q['q'] &= db.samples[i] != 'N/A'
        for i in ['dl', 'ul']:
            q['q'] &= db.samples[i] > 0
        try:
            r = db(q['q']).select(*q['s'])
            if r: r.export_to_csv_file(open(filename, 'wb'), dialect='excel', delimiter=';', lineterminator='b')
            QApplication.postEvent(self, ZEvent(lambda: self.setPB(100, filename, 100)))
        except Exception as ex:
            caption = 'Warning'
            desc =  """
            Error: %s

            You did sth wrong!

            """ % ex
            QApplication.postEvent(self, ZEvent(lambda: self.error(caption, desc)))
            QApplication.postEvent(self, ZEvent(lambda: self.setPB(0, filename)))

    def setPB(self, p, desc=None, max=None):
        if desc: self.parent.ui.progressBar_export.setFormat('Job: ' + desc + ' - %p%')
        if max: self.parent.ui.progressBar_export.setRange(0,max)
        self.parent.ui.progressBar_export.setValue(p)

    def export2kml(self):
        filename = self.parent.ui.lineEdit_exportFile.text()
        QApplication.postEvent(self, ZEvent(lambda: self.setPB(0, filename, 100)))
        q = self.query()
        try:
            kml = KMLFile(self, filename, q)
            QApplication.postEvent(self, ZEvent(lambda: self.setPB(100, filename, 100)))
        except Exception as ex:
            caption = 'Error occured'
            desc = "Error: %s" % ex
            QApplication.postEvent(self, ZEvent(lambda: self.error(caption, desc)))
            QApplication.postEvent(self, ZEvent(lambda: self.setPB(0, filename)))

    def query(self):
        query = []

        c =  self.parent.ui.checkBox_exportComputerID
        c2 = self.parent.ui.comboBox_exportComputerID
        if c.checkState() == Qt.Checked: query.append((self.db.samples.computer_id == c2.currentText()))

        p  = self.parent.ui.checkBox_exportProject
        p2 = self.parent.ui.comboBox_exportProject
        if p.checkState() == Qt.Checked: query.append((self.db.samples.project_name == p2.currentText()))

        f  = self.parent.ui.checkBox_exportFrom
        f2 = self.parent.ui.dateTimeEdit_exportFrom
        if f.checkState() == Qt.Checked: query.append((self.db.samples.utc >= f2.dateTime().toPython()))

        t  = self.parent.ui.checkBox_exportTo
        t2 = self.parent.ui.dateTimeEdit_exportTo
        if t.checkState() == Qt.Checked: query.append((self.db.samples.utc <= t2.dateTime().toPython()))

        to_select = []
        columns = self.parent.ui.listWidget_exportColumns
        for i in range(columns.count()):
            if  columns.item(i).checkState() == Qt.Checked:
                to_select.append(str('samples.%s' % columns.item(i).text()))

        if len(query) == 0:
            return dict(q=(self.db.samples.id > 0), s=to_select)
        return dict(q=reduce(lambda a, b:(a & b), query), s=to_select)

    def customEvent(self, event):
        if callable(event.callback):
            event.callback()

from lxml import etree
from pykml.factory import KML_ElementMaker as KML
from datetime import datetime
from com import convert_coords
from distance import points2distance
import re
import zipfile
class KMLFile(object):
    def __init__(self, parent, filename, q):
        self.parent = parent
        self.db = db = LocalStorage().db
        self.query = q
        main = KML.Folder(filename)
        self.th = 7
        self.margin = 0.5
        self.reCellID = re.compile(r'(.*),\sID:\s(\d*),\sSector:\s(\d*)')


        for i in range(self.th):
            main.append(KML.Style(
                KML.IconStyle(
                    KML.color(hexcolor(i,100,self.th)),
                    KML.scale(.5),
                    KML.Icon(
                        KML.href("sample.png"),
                    ),
                    KML.hotSpot(x="0.5",y="0.5",xunits="fraction",yunits="fraction"),
                ),
                id="sample_%s" % i))
            main.append(KML.Style(
                KML.LineStyle(
                    KML.width('2'),
                    KML.color(hexcolor_bts(i,self.th)),
                ),
                id="btsline_%s" % i))

        main.append(KML.Style(
                KML.IconStyle(
                    KML.scale(1),
                    KML.Icon(
                        KML.href("warning.png"),
                    ),
                    KML.hotSpot(x="0.5",y="0.5",xunits="fraction",yunits="fraction"),
                ),
                id="noservice"))
        main.append(KML.Style(
                KML.IconStyle(
                    KML.scale(3),
                    KML.Icon(
                        KML.href("tower.png"),
                    ),
                    KML.hotSpot(x="0.5",y="0.2",xunits="fraction",yunits="fraction"),
                ),
                id="bts"))

        if 'samples.dl' in q['s']: main.append(self.map('dl'))
        if 'samples.ul' in q['s']: main.append(self.map('ul'))
        if 'samples.speed' in q['s']: main.append(self.map('speed'))
        if 'samples.rssi' in q['s']: main.append(self.map('rssi'))
        if 'samples.rsrp' in q['s']: main.append(self.map('rsrp'))
        if 'samples.rsrq' in q['s']: main.append(self.map('rsrq'))
        if 'samples.distance' in q['s']: main.append(self.bts_map())
        if 'samples.distance' in q['s']: main.append(self.map('distance'))

        # f = open(filename, 'w')
        # f.write(etree.tostring(main))
        # f.close()
        with zipfile.ZipFile('%s/logs/%s.kmz' % (WD, filename), 'w', zipfile.ZIP_DEFLATED) as myzip:
            myzip.writestr(filename+'.kml', etree.tostring(main))
            myzip.write(WD+'/ge/sample.png', 'sample.png')
            myzip.write(WD+'/ge/warning.png', 'warning.png')
            myzip.write(WD+'/ge/tower.png', 'tower.png')
            myzip.close()

    def map(self, prop='dl'):
        x = prop

        db = self.db
        query = self.query['q']
        for i in ['rssi', 'rsrq', 'rsrp']:
            query &= db.samples[i] != 'N/A'
        if x in ['dl', 'ul']:
            query &= db.samples[x] > 0
        fld = KML.Folder(KML.name(x.upper()))
        f = dict()

        samples = db(query).select(db.samples[x],
                                   db.samples.lat,
                                   db.samples.lat_dir,
                                   db.samples.lon,
                                   db.samples.lon_dir,
                                   db.samples.cellid,
                                   db.samples.internet,
                                   db.samples.sample,
                                   *self.query['s'])

        QApplication.postEvent(self.parent, ZEvent(
            lambda: self.parent.setPB(
                0, 'exporting %s' % x.upper(), len(samples))))
        avg = db.samples[x].avg()
        avg = db(query).select(avg).first()[avg]
        min = db.samples[x].min()
        min = db(query).select(min).first()[min]
        max = db.samples[x].max()
        max = db(query).select(max).first()[max]
        _avg = None  # for samples >2Mbps
        if prop in ['dl', 'ul']:
            min = 2
            _avg = db.samples[x].avg()
            _avg = db(query)(db.samples[x] > min).select(_avg).first()[_avg]

        i = 0
        for p in samples:
            # Coordinates
            c = convert_coords(p.lat, p.lat_dir, p.lon, p.lon_dir)

            # folder structure
            folder = None
            import re
            m = self.getCell(p.cellid)
            if m:
                b = m[1]
                s = m[2]
                if not f.has_key(b): f[b] = dict()
                if not f[b].has_key(s): f[b][s] = []
                folder = f[b][s]
            else:
                if not f.has_key(p.cellid): f[p.cellid] = []
                folder = f[p.cellid]

            if p[x] == None: continue
            # Placemark data
            href = "#sample_%d" % self.normalize(p[x], min, max, x, _avg)
            if not p.internet and x in ['dl', 'ul']:
                href = "#noservice"
            if p[x] > max - self.margin: name = '%.2f %s' % (p[x], getUnit(x))
            else: name = ''
            if p.sample: name = u'%s !' % name
            desc = 'AVERAGE: %.2f %s\n' % (avg, getUnit(x))
            for k in self.query['s']:
                if isinstance(p[k], (int, float, str)):
                    desc += k.replace('samples.','').replace('_',' ').upper() + ' : %s %s\n' % (str(p[k]), getUnit(k))

            folder.append(KML.Placemark(
                KML.name(name),
                KML.description(desc.decode('utf-8')),
                KML.styleUrl(href),
                  KML.Point(
                    KML.coordinates("%s, %s" % (str(c[1]), str(c[0])))
                  )
                ))
            QApplication.postEvent(self.parent, ZEvent(lambda: self.parent.setPB(i)))
            i += 1
        recAppend(fld, f)
        return fld

    def bts_map(self):
        db = self.db;

        query = self.query['q']
        fld = KML.Folder(KML.name('BTS'))

        QApplication.postEvent(self.parent, ZEvent(lambda: self.parent.setPB(0, 'preparing BTS map', 100)))

        f = dict()
        f['TOWERS'] = dict()
        f['LINES'] = dict()
        fields = db.bts_list.fields
        samples = db(query & (db.bts_list.cell_id == db.samples.site_id)).select(
                                                    db.samples.id,
                                                    db.samples.distance,
                                                    db.samples.lat,
                                                    db.samples.lat_dir,
                                                    db.samples.lon,
                                                    db.samples.lon_dir,
                                                    db.samples.cellid,
                                                    db.samples.access_technology,
                                                    db.bts_list.ALL)
        QApplication.postEvent(self.parent, ZEvent(lambda: self.parent.setPB(0, 'exporting BTS map', len(samples))))
        # print samples
        i = 0
        for p in samples:
            m = self.getCell(p.samples.cellid)
            if m:
                if p.bts_list.id == None:
                    print 'No BTS'
                    continue
                if p.bts_list.latitude_bts == '':
                    print 'No BTS coords in DB'
                    continue
                if db.samples.access_technology == 'E-UTRAN' and p.bts_list.pci == None:
                    print 'No BTS PCI or ACCESS TECHNOLOGY flag'
                    continue

                b = m[1]
                s = m[2]
                if not f['LINES'].has_key(b):
                    f['TOWERS'][b] = dict()
                    f['LINES'][b] = dict()
                if not f['LINES'][b].has_key(s):
                    f['TOWERS'][b][s] = [None]
                    f['LINES'][b][s]  = []
                towers = f['TOWERS'][b][s]
                lines  = f['LINES'][b][s]

                # LIST BTS
                lat = float(p.bts_list.latitude_bts.replace(',','.'))
                lon = float(p.bts_list.longitude_bts.replace(',','.'))
                desc = ''
                for k in fields:
                    if p.bts_list[k] == None: continue
                    desc += unicode(k.replace('bts_list.', '').replace('_',' ').upper() + ' : %s %s\n' % (str(p.bts_list[k]), getUnit(k)))
                towers[0] = KML.Placemark(
                    KML.description(desc),
                    KML.styleUrl('#bts'),
                      KML.Point(
                        KML.coordinates("%s, %s" % (lon, lat))
                      )
                    )
                c = convert_coords(p.samples.lat, p.samples.lat_dir,
                                    p.samples.lon, p.samples.lon_dir)
                distance = points2distance(
                                ((c[0],0,0),(c[1],0,0)),
                                ((lat,0,0),(lon,0,0))
                                )
                db(db.samples.id==p.samples.id).update(distance=round(distance,3))
                href = "#btsline_%d" % (int(b)%self.th)
                lines.append(KML.Placemark(
                    KML.styleUrl(href),
                    KML.name('%.2f %s' % (distance, getUnit('distance'))),
                    KML.LineString(
                        KML.extrude('1'),
                        KML.tessellate('1'),
                        KML.altitudeMode('relativeToGround'),
                        KML.coordinates("%s,%s,%s %s,%s,2" % (lon, lat, p.bts_list.height.replace(',','.'), c[1], c[0]))
                    )
                    ))
            else:
                continue
            QApplication.postEvent(self.parent, ZEvent(lambda: self.parent.setPB(i)))
            i += 1
        db.commit()
        recAppend(fld, f)
        return fld

    def normalize(self, value, _min, _max, propname=None, _avg=None):
        if _avg:
            _max = _avg
        try:
            v = ((value - _min) / (_max - _min)) * (self.th - 1)
            if propname in ['dl', 'ul']:
                if value < _min:
                    return 0
                if value > _max:
                    return self.th - 1
                else:
                    return max(1, math.fabs(v))
            return math.fabs(v)
        except:
            return 0

    def getCell(self, string):
        m = self.reCellID.search(str(string))
        if m: return (m.group(1), m.group(2), m.group(3))
        else: return None

def getUnit(param):
    if 'dl' in param or 'ul' in param: return '[Mb/s]'
    if 'rssi' in param or 'rsrp' in param: return '[dBm]'
    if 'speed' in param: return '[km/h]'
    if 'distance' in param: return '[km]'
    if 'height' in param: return '[m]'
    return ''

def recAppend(fld, d):
    for k,v in d.items():
        f = KML.Folder(KML.name(k))
        if type(v) is dict:
            recAppend(f, v)
        elif type(v) is list:
            f = KML.Folder(KML.name(k), *v)
        else:
            f.append(v)
        fld.append(f)

def hexcolor(value, min=0, max=150):
    from colorsys import hsv_to_rgb
    h = float(((value * 240) / (max-1))) / 360
    r, g, b = hsv_to_rgb(h,1,1)
    return 'ff%02X%02X%02X' % (int(b*255), int(g*255), int(r*255))

def hexcolor_bts(value, m):
    value = value%m
    from colorsys import hls_to_rgb
    h = float(((value * 240) / (m-1))) / 360
    r, g, b = hls_to_rgb(h,0.9,1)
    return 'ff%02X%02X%02X' % (int(b*255), int(g*255), int(r*255))
