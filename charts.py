#!/usr/bin/python
# -*- coding: utf-8 -*-

from PySide.QtCore import *
from PySide.QtWebKit import *

from idle_dispatcher import ThreadDispatcher
from idle_queue import *
from ge import SautWebPage

class Charts(QObject):
    def __init__(self, parent):
        QObject.__init__(self, parent)
        self.my_hub = None

        self.parent = parent
        self.page = SautWebPage(self.parent.ui.tab_5)
        self.page.loadFinished.connect(self.onLoad)
        self.page.settings().setAttribute(QWebSettings.PluginsEnabled, True);
        self.parent.ui.webView_charts.setPage(self.page)
        self.page.mainFrame().load(QUrl('ge/charts.html'))

    def onLoad(self):
        self.my_hub = Hub()
        self.myFrame = self.page.mainFrame()
        self.myFrame.addToJavaScriptWindowObject("my_hub", self.my_hub)
        self.myFrame.evaluateJavaScript("initBridge();")

class Hub(QObject): 
    def __init__(self):
        super(Hub, self).__init__()
  
    def update(self, name, value):
        self.on_update.emit(name, value)
   
    on_update = Signal(str, float)