#!/usr/bin/python
# -*- coding: utf-8 -*-

from PySide.QtCore import *
import re

from com import *
from net import WebDataMiner
from idle_queue import *
from idle_dispatcher import *
from distance import points2distance


class ModemTracker(object):
    def start(self):
        raise NotImplementedError

    def stop(self):
        raise NotImplementedError

    def parse(self):
        raise NotImplementedError


class Modem(ModemTracker, QObject):
    INTERVAL = 500

    def __init__(self, parent=None, port=None, id=None):
        QObject.__init__(self)
        self.parent = parent
        self.port = port
        self.id = id
        self.data = dict()
        self.status = None
        self.current = ''

        self.timer = QTimer(self)
        self.timer.timeout.connect(self.sendCmd)
        self.cmds = []
        self.index = 0

        self.at = None
        self.dm = None

        self.r = [
            dict(re=re.compile(r"CREG:(.*)",
                 re.IGNORECASE), f=self._creg),
            dict(re=re.compile(r"CSQ:(.\d*),(.\d*)",
                 re.M), f=self._csq),
            dict(re=re.compile(r"EARFCN(\s:|=)(.?\d*)",
                 re.M), f=self._atel_earfcn),
            dict(re=re.compile(r"RSRP(\s:|=)(.?\d*)",
                 re.M), f=self._atel_rsrp),
            dict(re=re.compile(r"RSRQ(\s:|=)(.?\d*)",
                 re.M), f=self._atel_rsrq),
            dict(re=re.compile(r"RSSI(\s:|=)(.?\d*)",
                 re.M), f=self._atel_rssi),
            dict(re=re.compile(r"PCI(\s:|=)(.?\d*)",
                 re.M), f=self._atel_pci),
            dict(re=re.compile(r"SINR(\s:|=)(.?\d*)",
                 re.M), f=self._atel_sinr),
            dict(re=re.compile(r"Cell\sID(\s:|=)(.*?)<",
                 re.M), f=self._atel_cellid),
            dict(re=re.compile(r"\^LTERSRP:(.\d*),(.\d*)",
                 re.IGNORECASE), f=self._hw_rsrp_rsrq),
            dict(re=re.compile(r"\^RSSI:(.\d*)",
                 re.IGNORECASE), f=self._hw_rssi),
            dict(re=re.compile(r"\^LTESCINFO:(.\d*)",
                 re.IGNORECASE), f=self._hw_sc),
            dict(re=re.compile(r"CGMR\n(.*)",
                 re.M), f=self._cgmr),
            dict(re=re.compile(r"CGMI\n(.*)",
                 re.M), f=self._cgmi),
            dict(re=re.compile(r"^(<.*?)$",
                 re.DOTALL), f=self._xml),
            dict(re=re.compile(
                 r"Signal Quality: RSRP = (.\d*), RSRQ = (.\d*), SINR = (.\d*), RSSI = (.\d*)",
                 re.M), f=self._altair_meas8),
        ]

    def start(self):
        if self.id == 'HW E398' or self.id == 'HW E3272':
            self.timer.start(Modem.INTERVAL)
            self.at = AT(self, com=self.port, baudrate=9600)
            self.cmds = ['AT+CREG?', 'AT+CSQ', 'AT^LTESCINFO?', 'AT^LTERSRP?']
            self.at.start()
            self.at.add('E1', True)
            self.at.add('F1', True)
            self.at.add('AT+CREG=2', True)
        if self.id == 'Altair':
            self.timer.start(Modem.INTE100RVAL)
            self.at = AT(self, com=self.port, baudrate=9600)
            self.cmds = ['AT+CREG?', "AT%MEAS=\"8\""]
            self.at.star00t()
            self.at.add('E1', True)
            self.at.add('F1', True)
            self.at.add('AT+CREG=2', True)
        if self.id == 'ATEL ODU':
            self.timer.start(Modem.INTERVAL)
            self.at = AT(self, com=self.port, baudrate=921600)
            self.cmds = ['atc 1 AT+CREG?', 'atc 1 AT+CSQ']
            self.at.start()
            self.at.add('ltecm', True)
            self.at.add('rfrm 1 1', True)
            self.at.add('atc 1 AT+CREG=2', True)
            self.dm = WebDataMiner(
                self,
                'http://192.168.100.1/cgi-bin/ltestatus.cgi?Command=Status')
            self.dm.start()
        if self.id == 'ATEL ODU100/300 WEB':
            self.dm = WebDataMiner(
                self,
                'http://192.168.100.1/cgi-bin/ltestatus.cgi?Command=Status')
            self.dm.start()
        if self.id == 'ATEL ODU200 WEB':
            self.dm = WebDataMiner(
                self,
                'http://192.168.100.1/fcgi/setup.fcgi?cmd=Status')
            self.dm.start()

    def stop(self):
        if self.at:
            self.at.stop()
        if self.dm:
            self.dm.stop()
        self.timer.stop()

    def parse(self, s):
        self.current = s
        QApplication.postEvent(self.parent, ZEvent(self.parent.updateUI))
        for r in self.r:
            m = r['re'].search(s)
            if m:
                r['f'](m)
                self.status = 1
                QApplication.postEvent(
                    self.parent,
                    ZEvent(lambda: self.update(self.status)))

    def sendCmd(self):
        if self.at:
            self.at.add(self.cmds[self.index])
        self.index += 1
        self.index = self.index % len(self.cmds)

    def update(self, status):
        _l = self.parent.ui.label_modem
        if status == 0:
            _l.setText('Modem: Not connected!')
        if status == -1:
            _l.setText('Modem: Can\'t connect!')
        if status == -2:
            _l.setText('Modem: Connection lost!')
        if status == -3:
            _l.setText('Modem: No data!')
        if status == 1:
            _l.setText('Modem: OK')
        self.status = status

        # lte params
        table = self.parent.ui.tableWidget_LTEp
        if status < 1:
            table.clear()
            self.data.clear()
        else:
            data = self.data.copy()
            table.horizontalHeader().setStretchLastSection(True)
            table.setHorizontalHeaderItem(0, QTableWidgetItem('Parameter'))
            table.setHorizontalHeaderItem(1, QTableWidgetItem('Value'))
            table.setRowCount(len(data))
            table.setColumnCount(2)
            row_nb = 0
            for k in data:
                param = str(data[k])
                if k in ['Distance']:
                    param += ' [km]'
                if k in ['RSRP', 'RSSI']:
                    param += ' [dBm]'
                item1 = QTableWidgetItem(k)
                item2 = QTableWidgetItem(param)
                item1.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
                item2.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
                table.setItem(row_nb, 0, item1)
                table.setItem(row_nb, 1, item2)
                row_nb += 1

    def _hw_rsrp_rsrq(self, m):
        self.data['RSRP'] = m.group(1)
        self.data['RSRQ'] = m.group(2)

    def _atel_earfcn(self, m):
        self.data['EARFCN'] = m.group(2)

    def _atel_rsrp(self, m):
        self.data['RSRP'] = m.group(2)

    def _atel_rsrq(self, m):
        self.data['RSRQ'] = m.group(2)

    def _atel_pci(self, m):
        self.data['PCI'] = m.group(2)

    def _atel_rssi(self, m):
        self.data['RSSI'] = m.group(2)

    def _atel_sinr(self, m):
        self.data['SINR'] = m.group(2)

    def _atel_cellid(self, m):
        v = m.group(2)
        self.data['CellID'] = '0x%s, ID: %s, Sector: %s' % \
            (v, int(v[:-2], 16), v[-2:])
        self.data['SITE ID'] = int(v[:-2], 16)

    def _hw_sc(self, m):
        v = m.group(1).split(', ')
        self.data['PCI'] = v[0]

    def _hw_rssi(self, m):
        try:
            v = m.group(1)
            if v == 0:
                self.data['RSSI'] = -113
            elif v == 1:
                self.data['RSSI'] = -111
            elif v == 31:
                self.data['RSSI'] = -51
            elif v == 99:
                self.data['RSSI'] = 99
            else:
                rssi = -113 + (2 * float(v.replace(', ', '.')))
                self.data['RSSI'] = rssi
            return self.data['RSSI']
        except Exception as ex:
            pass#print 'ERROR:', ex

    def _creg(self, m):
        v = m.group(1)
        v = v.replace(' ', '')
        v = v.replace('"', '')
        v = v.split(', ')
        if len(v) >= 5:
            code = int(v[1])
            info = ''
            if code == 0:
                info = 'Not registered, not searching'
            elif code == 1:
                info = 'Registered to home network'
            elif code == 2:
                info = 'Not registered, searching for network'
            elif code == 3:
                info = 'Registration denied'
            elif code == 4:
                info = 'out of coverage?'
            elif code == 5:
                info = 'Registered, roaming'
            elif code == 6:
                info = 'Registered, SMS only, home'
            elif code == 7:
                info = 'Registered, SMS only, roaming'
            elif code == 8:
                info = 'Registered, emergency only'
            elif code == 9:
                info = 'Registered for "CSFB not preferred", home'
            elif code == 10:
                info = 'Registered for "CSFB not preferred", roaming'
            else:
                info = 'Unknown: %s' % code
            self.data['Status'] = info

            try:
                self.data['CellID'] = '0x%s, ID: %s, Sector: %s' % \
                    (v[3], int(v[3][:-2], 16), v[3][-2:])
                self.data['SITE ID'] = int(v[3][:-2], 16)
            except:
                print 'CREG parsing warning'

            try:
                db = self.parent.db
                gps = self.parent.gps
                if gps.status == 1:
                    if 'GPRMC' in gps.data:
                        if gps.data['GPRMC'].data_validity == 'A':
                            bts = db.bts_list
                            row = db(
                                bts.cell_id == self.data['SITE ID']).select(
                                bts.id, bts.latitude_center,
                                bts.longitude_center)
                            if row:
                                car = convert_coords(
                                    gps.data['GPRMC'].lat,
                                    gps.data['GPRMC'].lat_dir,
                                    gps.data['GPRMC'].lon,
                                    gps.data['GPRMC'].lon_dir)
                                lat = float(
                                    row[0].latitude_center.replace(', ', '.'))
                                lon = float(
                                    row[0].longitude_center.replace(', ', '.'))
                                distance = points2distance(
                                    ((car[0], 0, 0), (car[1], 0, 0)),
                                    ((lat, 0, 0), (lon, 0, 0)))
                                self.data['Distance'] = '%.3f' % distance
                else:
                    if 'Distance' in self.data:
                        del self.data['Distance']

                code = int(v[4])
                info = ''
                if code == 0:
                    info = 'GSM'
                elif code == 1:
                    info = 'GSM Compact'
                elif code == 2:
                    info = 'UTRAN'
                elif code == 3:
                    info = 'GSM w/EGPRS'
                elif code == 4:
                    info = 'UTRAN w/HSDPA'
                elif code == 5:
                    info = 'UTRAN w/HSUPA'
                elif code == 6:
                    info = 'UTRAN w/HSDPA&HSUPA'
                elif code == 7:
                    info = 'E-UTRAN'
                self.data['Access Technology'] = info

                if code == 7:
                    self.data['TAC'] = '0x%s, %s' % (v[2], int(v[2], 16))
                    self.data['TAC']
                else:
                    self.data['LAC'] = '0x%s, %s' % (v[2], int(v[2], 16))
            except Exception as ex:
                print 'ERROR:', ex

    def _cgmr(self, m):
        self.data['CGMR'] = m.group(1)

    def _cgmi(self, m):
        self.data['CGMI'] = m.group(1)

    def _csq(self, m):
        if m.group(1):
            self.data['RSSI'] = self._hw_rssi(m)
        if m.group(2):
            self.data['BER'] = m.group(2)

    def _rssi(self, m):
        self.data['RSSI'] = m.group(1)

    def _xml(self, m):
        from lxml import etree
        try:
            root = etree.fromstring(m.group(1))
            self.data['EARFCN'] = root.xpath('//EARFCN')[0].text
            self.data['RSSI'] = root.xpath('//RSSI')[0].text
            self.data['RSRP'] = root.xpath('//RSRP')[0].text
            self.data['RSRQ'] = root.xpath('//RSRQ')[0].text
            self.data['PCI'] = int(root.xpath('//PhyCellID')[0].text, 16)
            try:
                self.data['Cell desc'] = root.xpath('//CellGlobalID')[0].text
                v = hex(int(self.data['Cell desc']))
                self.data['CellID'] = '0x%s, ID: %s, Sector: %s' % \
                    (v[2:], int(v[:-2], 16), v[-2:])
                self.data['SITE ID'] = int(v[:-2], 16)
            except ValueError:
                v = hex(int(self.data['Cell desc'][5:], 16))
                self.data['CellID'] = '0x%s, ID: %s, Sector: %s' % \
                    (v[2:], int(v[:-2], 16), v[-2:])
                self.data['SITE ID'] = int(v[:-2], 16)
            except:
                pass

            self.data['SINR'] = root.xpath('//SINR')[0].text
        except Exception as ex:
            print 'ERROR:', ex

    def _altair_meas8(self, m):
        if m.group(1):
            self.data['RSRP'] = m.group(1)
        if m.group(2):
            self.data['RSRQ'] = m.group(2)
        if m.group(3):
            self.data['SINR'] = m.group(3)
        if m.group(4):
            self.data['RSSI'] = m.group(4)
