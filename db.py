#!/usr/bin/python
# -*- coding: utf-8 -*-

from dal import DAL, Field
from datetime import datetime
import os

DATABASE = 'sqlite://storage.sqlite'


class LocalStorage(object):
    def __init__(self):
        if not os.path.exists('logs'):
            os.makedirs('logs')
        self.db = DAL(DATABASE, folder='logs', db_codec='UTF-8')

        fields = [
            Field('utc', 'datetime', default=datetime.utcnow()),
            Field('computer_id', 'string'),
            Field('project_name', 'string'),
            Field('lat', 'string'),
            Field('lat_dir', 'string'),
            Field('lon', 'string'),
            Field('lon_dir', 'string'),
            Field('speed', 'float'),
            Field('dl', 'float'),
            Field('ul', 'float'),
            Field('internet', 'boolean'),
            Field('sample', 'string'),
            Field('desc', 'text'),
            # extra fields
            Field('earfcn', 'integer'),
            Field('rsrp', 'float'),
            Field('rsrq', 'float'),
            Field('rssi', 'float'),
            Field('pci', 'integer'),
            Field('status', 'string'),
            Field('cell_desc', 'string'),
            Field('cellid', 'string'),
            Field('site_id', 'string'),
            Field('distance', 'float'),
            Field('access_technology', 'string'),
            Field('tac', 'string'),
            Field('lac', 'string'),
            Field('ber', 'integer'),
            Field('sinr', 'integer'),
            Field('att01', 'integer'),
            Field('att02', 'integer'),
            Field('att03', 'integer'),
            Field('att04', 'integer')
        ]

        self.db.define_table('samples', *fields)

        # fields = [
        #     Field('zone', 'string'),
        #     Field('name', 'string'),
        #     Field('site4g_name', 'string'),
        #     Field('a2_name', 'string'),20\
        #     Field('address', 'string'),
        #     Field('enodeb_id', 'string'),
        #     Field('tac', 'string'),
        #     Field('pci', 'string'),
        #     Field('bearing', 'string'),
        #     Field('height', 'string'),
        #     Field('on_air', 'string'),
        #     Field('numlatitude', 'string'),
        #     Field('numlongitude', 'string'),
        #     Field('day', 'string')
        # ]

        fields = [
            Field('cell_name_plk', 'string'),
            Field('site_id_plk', 'string'),
            Field('site_id_a2', 'string'),
            Field('site_name', 'string'),
            Field('rnc_name', 'string'),
            Field('mme', 'string'),
            Field('tac', 'string'),
            Field('lac', 'string'),
            Field('cell_id', 'integer'), # enodeb_id -> cell_id
            Field('sector_id', 'integer'), # sektor_id -> sector_id, sektor_nr -> sector_id
            Field('pci', 'integer'),
            Field('psc', 'integer'),
            Field('height', 'string'),
            Field('azimuth', 'integer'), # azymut -> azimuth
            Field('operator', 'string'),
            Field('data', 'string'),
            Field('longitude_center', 'string'),
            Field('latitude_center', 'string'),
            Field('longitude_bts', 'string'),
            Field('latitude_bts', 'string'),
            Field('beam_angle', 'string'),
            Field('radius', 'integer')
        ]
        try:
            self.db.executesql('CREATE INDEX IF NOT EXISTS bts_idx ON bts_list (cell_id);')
        except:
            pass

        self.db.define_table('bts_list', *fields)
